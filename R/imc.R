# WARNING - Generated by {fusen} from /dev/flat_full.Rmd: do not edit by hand

#' imc
#' 
#' Cette fonction imc() retourne l’Indice de masse corporelle d’un individu
#' 
#' @param masse la masse en kg
#' @param poids la taille en m
#' 
#' @importFrom dplyr mutate
#' 
#' @return une valeur numérique supérieure à 0
#' 
#' @export 
#' @examples
#' library(dplyr)
#' imc(masse = 80, taille = 1.8)
#' 
#' 
#' data("donnees_poids")
#' donnees_poids %>%
#'   na.omit() %>%
#'   mutate(imc = imc(masse = mass,taille = height))
imc <- function(masse, taille){
  
  masse <- as.numeric(masse, replace=TRUE)
  taille <- as.numeric(taille, replace=TRUE)
  
  x = ((masse) / (taille ^2))
  
  return(x)
    
}
